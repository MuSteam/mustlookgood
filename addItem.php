<?php
session_start();
?>
<!DOCTYPE html>

<?php
  require_once( "objects/Item.php");
  require_once( "objects/Clothing.php");
  require_once( "connect.php");

?>

<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="static/css/general.css" />
    <link rel="stylesheet" type="text/css" href="static/css/addItem.css" />

    <script src="static/libs/jquery-2.1.4.js" ></script>

    <link rel='stylesheet' href='static/libs/spectrum/spectrum.css' />
    <script src='static/libs/spectrum/spectrum.js'></script>

    <title>Add Item</title>
  </head>

  <body>
    <div id="mainContainer">
      <div id='containerCenter'>
        <img src="static/img/beeldmerk_MuStLG.png" width="15%" align="right">
        <div class="form">
          <h1>Upload een item</h1>
          <h5><b class="requiredStar">*</b> verplicht</h5>
          <form method="post" enctype="multipart/form-data" action="itemAdded.php">
            <p><input class="input" type="text" name="brand" placeholder="Merk" required><b class="requiredStar"> *</b></p>
            
            <script>
              $(document).ready( function () {
                $('#alert').hide();
              });
              var selected = false;
              function select(item) {
                $('img').each( function () {
                  var img = $(this).attr('src');
                  var string = img.replace("_selected", "");
                  $(this).attr('src', string);
                });
                if($('.'+item+'').attr('src') === 'static/img/icon_'+item+'.png')
                {
                  $('.'+item+'').attr('src', 'static/img/icon_'+item+'_selected.png');
                  $('.'+item+'Input').prop('checked', true);
                  selected = true;
                  $('#alert').hide();
                }
                else
                {
                  $('.'+item+'').attr('src', 'static/img/icon_'+item+'.png');
                  $('.'+item+'Input').prop('checked', false);
                  selected = false;
                }
              }
              function validate()
              {
                if(selected)
                {
                  $('form').submit();
                }
                else
                {
                  $("#alert").html('U heeft nog geen kledingtype aangeklikt').show();
                  
                }
              }
            </script>

            <input class="imgCheckbox shirtInput" type="checkbox" name="shirt"/>
            <input class="imgCheckbox truiInput" type="checkbox" name="trui"/>
            <input class="imgCheckbox broekInput" type="checkbox" name="broek"/>
            <input class="imgCheckbox jasInput" type="checkbox" name="jas"/>
            <input class="imgCheckbox hoofddekselInput" type="checkbox" name="hoofddeksel"/>
            <input class="imgCheckbox overhemdInput" type="checkbox" name="overhemd"/>
            <input class="imgCheckbox schoenenInput" type="checkbox" name="schoenen"/>
            
            <div class="imgDiv">
            <img class="img shirt" src="static/img/icon_shirt.png" onclick="select('shirt')" alt="Shirt" title="Shirt"/>
            <img class="img trui" src="static/img/icon_trui.png" onclick="select('trui')" alt="Trui" title="Trui"/>
            <img class="img broek" src="static/img/icon_broek.png" onclick="select('broek')" alt="Broek" title="Broek"/>
            <img class="img jas" src="static/img/icon_jas.png" onclick="select('jas')" alt="Jas" title="Jas"/>
            </div>
            <div class="imgDiv">
            <img class="img hoofddeksel" src="static/img/icon_hoofddeksel.png" onclick="select('hoofddeksel')" alt="Hoofddeksel" title="Hoofddeksel"/>
            <img class="img overhemd" src="static/img/icon_overhemd.png" onclick="select('overhemd')" alt="Overhemd" title="Overhemd"/>
            <img class="img schoenen" src="static/img/icon_schoenen.png" onclick="select('schoenen')" alt="Schoenen" title="Schoenen"/>
            <b class="requiredStar"> * </b>
            </div>
            <div style="clear:both"></div>
            <div id="alert" style="height:30px"></div>
            
            <!--p><input class="input" type="text" name="type" placeholder="Type   ( broek / shirt / rok )" required><b class="requiredStar">*</b></p-->

            <p><input class="input" type="text" name="subtype" placeholder="Subtype   ( korte broek / driekwart broek )"><br></p>
            
            <div style="position: relative">
              <input id="showPaletteOnly" type="text" name="color" style="float:left" value="green" readonly required>
              <b class="requiredStar" > *</b>
              <span class="color">Kleur</span>
              <div style="clear:both"></div>
            </div>
            
            <p><label class="select"><select style="width: 172px" class="select" name="style" required>
                  <option  value="Arty">Arty</option>
                  <option  value="Chic">Chic</option>
                  <option  value="Classic">Classic</option>
                  <option  value="Casual">Casual</option>
                  <option  value="Exotic">Exotic</option>
                  <option  value="Sophisticated">Sophisticated</option>
                  <option  value="Western">Western</option>
                  <option  value="Traditional">Traditional</option>
                  <option  value="Punk">Punk</option>
                  <option  value="Rocker">Rocker</option>
                  <option  value="Gothic">Gothic</option>
               </select></label><b class="requiredStar"> *</b>
            <!--p><input class="input" type="text" name="style" placeholder="Style   ( casual / blacktie / cocktail )" required><b class="requiredStar"> *</b></br-->

            <p><label class="select"><select style="width: 172px" class="select" name="season" required>
                  <option  value="all">Alle seizoenen</option>
                  <option  value="zomer">Zomer</option>
                  <option  value="herfst">Herfst</option>
                  <option  value="winter">Winter</option>
                  <option  value="lente">Lente</option>
               </select></label><b class="requiredStar"> *</b>
            <p><input class="input" type="text" name="material" placeholder="Materiaal   ( katoen / wol )"></p>
            <p><input class="input" type="text" name="dateOfPurchase" placeholder="Aankoopdatum   ( dd-mm-yyyy )" pattern="[0-3]{1}[0-9]{1}-[0|1]{1}[0-9]{1}-[1|2]{1}[0|9]{1}[0-9]{2}" title="dd-mm-yyyy" ></p>
            <script>
              $(document).ready( function () {
                $("#showPaletteOnly").spectrum({
                  preferredFormat: "name",
                  showPaletteOnly: true,
                  showPalette: true,
                  allowEmpty: false,
                  color: "green",
                  palette: [
                      ['black', 'navy', 'maroon', 'purple', 'olive'],
                      ['grey', 'blue', 'brown', 'fuchsia', 'green'],
                      ['silver', 'teal', 'orange', 'violet', 'lime'],
                      ['white', 'aqua', 'red', 'pink', 'yellow']
                  ]
                });
                $(".browseBtn").click( function () { //Bij het klikken op de browseBtn, wordt er hiermee op de input-type=file btn geklikt
                  $("#uploadInput").click();
                });
                $("#uploadInput").change( function () { //Als er een bestand is gekozen, wordt de naam in een andere textbox weergegeven
                  $("#fileNameBox").val(this.files[0].name);
                });
                $('.clearBtn').click( function () { //Wis de tekst in de textbox met de naam van het bestand
                  $('#fileNameBox').val("");
                  $('#uploadInput').val("");
                });
                // IK PROBEER FOCUS OP DE KLEURENBOX TE KRIJGEN VIA DE TAB-TOETS MAAR HET LUKT NIET!
                //$(".sp-replacer").attr("tabindex","4").focus();
              });
              /*$('body').live('keydown', function(e) {
                if (is_dialog_visible) {
                  var keyCode = e.keyCode || e.which;
                  if (keyCode === 2) {
                    e.preventDefault();
                    if (container.find('[name="subtype"]').is(":focus")) {
                      container.find(".sp-replacer").focus();
                    } else {
                      container.find(".sp-replacer").focus();
                    }
                  }
                }
              });
              /*$('body').live("keydown", function(e) {
                  if($("[name='subtype']").is(":focus"))
                  {
                    if (is_dialog_visible) {
                      var keyCode = e.keyCode || e.which;
                      if (keyCode === 27) {
                        e.preventDefault();
                        $("sp-replacer").focus();
                      }
                    }
                  }
                });*/
            </script>
            <div class="fileUploadDiv">
              <input id="fileNameBox" type="text" placeholder="Geen bestand gekozen" disabled >
              <input class="browseBtn btn" type="button" value="Bladeren">
              <input id="uploadInput" type="file" name="receipt" accept=".png,.jpg,.jpeg" readonly >
              <input class="clearBtn btn" type="button" value="Wis">
            </div>
            <div style="clear:both"></div>

            <p><input class="input" type="text" name="description" placeholder="Omschrijving"></p>
            <p><label class="select"><select style="width: 172px" class="select" name="size">
                  <option  value="XS">XS</option>
                  <option  value="S">S</option>
                  <option  value="M">M</option>
                  <option  value="L">L</option>
                  <option  value="XL">XL</option>
                  <option  value="XXL">XXL</option>
               </select></label>
            <div class="available"><input type="checkbox" checked name="available" value=1>Momenteel beschikbaar</div>
            <p><input class="input" type="integer" name="price" placeholder="Prijs"></p>
            <input class="input submit" type="submit" name="addMore" value="Meer toevoegen"><br><br>
            <input class="input submit" type="submit" name="generate" value="Match kleding" onclick="validate()">
                     
          </form>
        </div>
        <a href="http://localhost/mustlookgood/login.php" class="logout">logout</a>
      </div>
    </div>
  </body>
</html>
