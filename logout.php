<?php
session_start();
require_once 'connect.php';
require_once 'objects\User.php';
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>logout</title>
    </head>
    <body>
        <?php
        $user = new must\User($conn);
        $user->logout();
        ?>
    </body>
</html>
