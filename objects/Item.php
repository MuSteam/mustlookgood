<?php
namespace must;

class Item
{
//props
  private $item_ID  = 0;
  
  protected
    
    $brand          = "Geen invoer",
    $type           = "Geen invoer",
    $subtype        = "Geen invoer",
    $color          = "Geen invoer",
    $style          = "Geen invoer",

    $material       = "Geen invoer",

    $dateOfPurchase = "Geen invoer",
    $receipt        = "blob",
    $description    = "Geen invoer",
    $available      = 0,
    $price          = 0.0,
    $size           = "M";

//create new item methods
  function __construct($conn)
  {
    $this->conn = $conn;
  }

  public function insert($brand, $type, $color, $style, $subtype, $material, $dateOfPurchase, $description, $price, $available, $size)
  {
    $this->type = addslashes($type);
    $this->color= $color;
    $this->style= addslashes($style);
    $this->brand= addslashes($brand);
    if($available)
    {
      $this->available = 1;
    }
    
    if (isset($subtype)) 
    {
      self::setSubtype($subtype);
    }
    if (isset($material)) 
    {
      self::setMaterial($material);
    }
    if (isset($dateOfPurchase)) 
    {
      self::setDateOfPurchase($dateOfPurchase);
    }
    if (isset($description)) 
    {
      self::setDescription($description);
    }
    if (isset($price)) 
    {
      self::setPrice($price);
    }
    if (isset($size)) 
    {
      self::setSize($size);
    }

    $sql = "INSERT INTO `must`.`item` (`type`, `color`, `style`, `brand`, `subtype`, `material`, `dateOfPurchase`, `description`, `price`, `available`, `size`)
            VALUES ( '$this->type', '$this->color', '$this->style', '$this->brand', '$this->subtype', '$this->material', '$this->dateOfPurchase', '$this->description', '$this->price', '$this->available', '$this->size')";
    try
    {
      $result = $this->conn->query($sql);
      $item_ID = $this->conn->insert_id;
      
      if($result){
        return $item_ID;
      }
    }
    catch(Exception $e)
    {
      return "Uw upload is helaas niet gelukt door een technische storing.<br>"
              . $e->getMessage()
              . "Klik <a href='addItem.php' title='item toevoegen'>hier</a> om opnieuw te proberen.";
    }
  }
  
  function getItemOnProperty ($propertyName, $property) {
    $sql = "SELECT * FROM item WHERE $propertyName = '$property'";
    
    $result = $this->conn->query($sql);
    
    return $result;
  }
  
  /*public function rgbToText($color)
  {
    switch($color)
    {
      case "rgb(0, 0, 0)":
        return "black";
      case "rgb(128, 0, 0)":
        return "maroon";
      case "rgb(0, 0, 128)":
        return "navy";
      case "rgb(128, 0, 128)":
        return "purple";
      case "rgb(128, 128, 0)":
        return "olive";
      case "rgb(128, 0, 128)":
        return "grey";
      case "rgb(0, 0, 255)":
        return "blue";
      case "rgb(165, 42, 42)":
        return "brown";
      case "rgb(255, 0, 255)":
        return "fuchsia";
      case "rgb(0, 128, 0)":
        return "green";
      case "rgb(192, 192, 192)":
        return "silver";
      case "rgb(0, 128, 128)":
        return "teal";
      case "rgb(255, 165, 0)":
        return "orange";
      case "rgb(238, 130, 238)":
        return "violet";
      case "rgb(0, 255, 0)":
        return "lime";
      case "rgb(255, 255, 255)":
        return "white";
      case "rgb(0, 255, 255)":
        return "aqua";
      case "rgb(255, 0, 0)":
        return "red";
      case "rgb(255, 192, 203)":
        return "pink";
      case "rgb(255, 255, 0)":
        return "yellow";
    }
  }*/


//getters
  public function getItem_Id() {
    return $this->item_ID;
  }

  public function getBrand() {
    return $this->brand;
  }

  public function getType() {
    return $this->type;
  }

  public function getSubtype() {
    return $this->subtype;
  }

  public function getColor() {
    return $this->color;
  }

  public function getStyle() {
    return $this->style;
  }

  public function getMaterial() {
    return $this->material;
  }

  public function getDateOfPerchase() {
    return $this->dateOfPerchase;
  }

  public function getReceipt() {
    return $this->receipt;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getAvailable() {
    return $this->available;
  }

  public function getPrice() {
    return $this->price;
  }

  public function getSize() {
    return $this->size;
  }

//setters
  private function setItem_ID($item_ID) {
    $this->item_ID = $item_ID;
  }
  
  public function setBrand($brand) {
    $this->brand = $brand;
  }

  public function setType($type) {
    $this->type = $type;
  }

  public function setSubtype($subtype) {
    $this->subtype = $subtype;
  }

  public function setColor($color) {
    $this->color = $color;
  }

  public function setStyle($style) {
    $this->style = $style;
  }

  public function setMaterial($material) {
    $this->material = addslashes($material);
  }

  public function setDateOfPurchase($dateOfPurchase) {
    $dateOfPurchase = preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1', $dateOfPurchase);
    $this->dateOfPurchase = $dateOfPurchase;
  }

  public function setReceipt($receipt) {
    $this->receipt = $receipt;
  }

  public function setDescription($description) {
    $this->description = $description;
  }

  public function setAvailable($available) {
    $this->available = $available;
  }

  public function setPrice($price) {
    $this->price = $price;
  }

  public function setSize($size) {
    $this->size = $size;
  }
}

?>
