<?php
namespace must;

class Clothing extends Item
{
  private $clothing_ID = 0;
  
  protected
    $pattern   	    = "NONE",
    $season         = "Geen invoer";

  function __construct($conn)
  {
    Item::__construct($conn);
  }

  //setters
  function setPattern($pattern) {
    $this->pattern = $pattern;
  }

  function setSeason($season) {
    $this->season = $season;
  }
  
  //other functions
  function insertClothing($user_ID, $season, $brand, $type, $color, $style, $subtype, $material, $dateOfPurchase, $description, $price, $available, $size)
  {
    $item_ID = parent::insert($brand, $type, $color, $style, $subtype, $material, $dateOfPurchase, $description, $price, $available, $size);
    
    $this->season = $season;

    $sql = "INSERT INTO `clothing`( `item_ID`, `pattern`, `season`, `user_ID`)
            VALUES ( '$item_ID', '$this->pattern', '$this->season', '$user_ID')";
    
    $result = $this->conn->query($sql);

    if($result)
    {
      return true;
    }
    else
    {
      throw new \Exception("Het uploaden is helaas niet gelukt door een technische storing."
              . "Klik <a href='addItem.php' title='Inloggen of registreren'>hier</a> om opnieuw te proberen.");
    }
  }
  
  function concatSqlQueryWithANDcomparison($propertyName, $property) {
    $concatQuery = isset($property) && $property ? "AND $propertyName = '$property'" : "";
    
    return $concatQuery;
  }
  
  function sqlSelectQueryOnConditions( $user_ID, $type, $style, $brand, $season ) {
    $brandComparisonConcatQuery = self::concatSqlQueryWithANDcomparison('brand', $brand);
    
    $sql = "SELECT clothing.item_ID, pattern FROM clothing "
                  . "INNER JOIN item ON item.item_ID = clothing.item_ID "
                  . "WHERE user_ID = '$user_ID' AND style = '$style' AND type = '$type' "
                  . "AND available = 1 AND (season = '$season' OR season = 'all')".$brandComparisonConcatQuery;
    $result = $this->conn->query($sql);
    
    return $result;
  }
  
  public function getOneItemOnConditions( $userId, $type, $style, $brand, $season ) {
    $properties = [];
    
    $result = self::sqlSelectQueryOnConditions( $userId, $type, $style, $brand, $season );
    
    if( $result->num_rows > 0 )
    {
      $i = 1;
      while( $row = $result->fetch_assoc() )
      {
        $itemIdArray[$i] = $row['item_ID'];
        $patternArray[$i] = $row['pattern'];
        $i++;
      }
      $amountOfItems = count( $itemIdArray );// 1 array tellen want alle arrays zijn van hetzelfde aantal
      
      // 1 random index aanmaken die dezelfde index moet zijn in de andere arrays omdat het allemaal op dezelfde volgorde in de arrays wordt gestopt
      $randomIndexOfItemsArray = mt_rand( 1, $amountOfItems );
      
      $itemId = $itemIdArray[$randomIndexOfItemsArray]; // random = bv 5 en 5 wordt dan voor alle arrays gebruikt
      $properties["pattern"] = $patternArray[$randomIndexOfItemsArray];
      
      $result1 = parent::getItemOnProperty( 'item_ID', $itemId );
      
      if( $result1->num_rows > 0 )
      {
        while( $row = $result1->fetch_assoc() )
        {
          $properties["brand"] = $row['brand'];
          $properties["subtype"] = $row['subtype'];
          $properties["color"] = $row['color'];
          $properties["description"] = $row['description'];
          $properties["size"] = $row['size'];
        }
      }
    }
    return $properties;
  }
}
?>
