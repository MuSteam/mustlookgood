<?php
namespace must;

class User
{
//props
  private $user_ID   = 0;
  
  protected
    $email          = "UNKNOWN",
    $dateOfBirth    = "",
    $name           = "UNKNOWN",
    $password       = "UNKNOWN",
    $subscribeDate  = "";
 
public function __construct($conn) {
  $this->conn = $conn;
}

public function insert($dateOfBirth, $email, $name, $password)
{
  if(preg_match("/^[a-zA-Z0-9._]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,8}$/", $email))
  {
    $this->email = $email;
  }
  else
  {
    throw new Exception("email invalid");
  }
  $this->name = addslashes($name);
  $dateOfBirth = preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1', $dateOfBirth);
  $this->dateOfBirth=$dateOfBirth;
  $this->password = addslashes($password);

  $sql = "INSERT INTO `must`.`users` ( `name`, `dateOfBirth`, `email`, `password`, `subscribeDate`)
          VALUES ( '$this->name', '$this->dateOfBirth', '$this->email', '$this->password', NOW())";
  try
  {
    $result = $this->conn->query($sql);
    $user_ID = $this->conn->insert_id;

    if($result){
      return $user_ID;
    }
  }
  catch(Exception $e)
  {
    return "Uw inschrijving is helaas niet gelukt door een technische storing.<br>"
            . $e->getMessage()
            . "Klik <a href='login.php' title='Inloggen of registreren'>hier</a> om opnieuw te proberen.";
  }
}

public function checkPassword($email, $password)
{
  try
  {
    $sql = "SELECT password FROM users WHERE email = '$email'";

    $result = $this->conn->query($sql);

    if($result->num_rows > 0)
    {
      while($row = $result->fetch_assoc())
      {
        $passwordDB = $row['password'];
      }
    }
    if(password_verify($password, $passwordDB))
    {
      return true;
    }
    else
    {
      return false;
    }
  } 
  catch (Exception $e) 
  {
    return "uw gebruikersnaam en of wachtwoord zijn niet juist."
            . $e->getMessage()
            . "Klik <a href='login.php' title='Inloggen of registreren'>hier</a> om opnieuw te proberen.";
  }  
}

public function retreiveIDByMail($email) 
{
  $sql = "SELECT user_ID FROM users WHERE email = '$email'";
  $result = $this->conn->query($sql);

  if($result->num_rows > 0)
    {
      while($row = $result->fetch_assoc())
      {
        $userId = $row['user_ID'];
      }
      return $userId;
    }
}

public function checkEmailExists($email)
{
  $sql = "SELECT email FROM users WHERE email = '$email'";
  $result = $this->conn->query($sql);

  if($result->num_rows > 0)
    {
      return TRUE;
    }
    else
    {
      return FALSE;
    }
}
/*public function retrieveUser($user_ID)
  {
    $sql = "SELECT * FROM users WHERE user_ID = $user_ID";
    
    try
    {
      // vraag aanbieden aan het dbms
      $result = $this->conn->query($sql);
      
      if($result->num_rows > 0)
      {
        while($rij = $resultaat->fetch_assoc())
        {
          $user_ID = $rij['user_ID'];
          $name = $rij['name'];
          $email = $rij['email'];
        }
        return $user_ID."<br>".$name."<br>".$email;
      }
    }
    catch(Exception $e)
    {
      return "Er is iets fout gegaan.<br>"
              . $e->getMessage()
              . "Klik <a href='inloggertje.php' title='Inloggen of registreren'>hier</a> om opnieuw te proberen.";
    }
    
    
  }*/
//setters

//getters
  public function getUser_ID() {
    return $this->user_ID;
  }

  public function getEmail() {
    return $this->email;
  }

  public function getName() {
    return $this->name;
  }
}
?>