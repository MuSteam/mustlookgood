<?php
session_start();

require_once 'connect.php';
require_once 'objects\User.php';

$user = new \must\User($conn);

$register = filter_input(INPUT_POST, 'register');

if(isset($register))
{
  $name = filter_input(INPUT_POST, 'name');
  $email = filter_input(INPUT_POST, 'email');
  $password = filter_input(INPUT_POST, 'password');
  $checkedPassword = password_hash($password, PASSWORD_DEFAULT);
  $dateOfBirth = filter_input(INPUT_POST, 'dateOfBirth');

  if ($user->checkEmailExists($email)== true)
  {
    $_SESSION["emailExists"] = true;
    header('location:login.php');
  }
  
  $user->insert($dateOfBirth, $email, $name, $checkedPassword);
}
$_SESSION["user_ID"] = $user->retreiveIDByMail($email);
header("Location: addItem.php");
?>