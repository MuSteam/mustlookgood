<?php
session_start();
?>
<html>
   <link rel="stylesheet" type="text/css" href="static/css/general.css" />
   <!--link rel="stylesheet" type="text/css" href="static/css/inloggertje.css" /-->

   <div id='containerCenter'>
<?php
require_once 'connect.php';
require_once 'objects\Item.php';
require_once 'objects\Clothing.php';
require_once 'objects\User.php';

//get type
  switch (isset($_POST)) {
    case isset($_POST['shirt']):
      $type="shirt";
      break;
    case isset($_POST['trui']):
      $type="trui";
      break;
    case isset($_POST['broek']):
      $type="broek";
      break;
    case isset($_POST['jas']):
      $type="jas";
      break;
    case isset($_POST['hoofddeksel']):
      $type="hoofddeksel";
      break;
    case isset($_POST['overhemd']):
      $type="overhemd";
      break;
    case isset($_POST['schoenen']):
      $type="schoenen";
      break;
  }

$addMore = filter_input(INPUT_POST, 'addMore');
$generate = filter_input(INPUT_POST, 'generate');

$season = filter_input(INPUT_POST, 'season');
$brand = filter_input(INPUT_POST, 'brand');
$color = filter_input(INPUT_POST, 'color');
$style = filter_input(INPUT_POST, 'style');
switch($style)
{
  case "Arty":
    $style = "Arty";
    break;
  case "Chic":
    $style = "Chic";
    break;
  case "Classic":
    $style = "Classic";
    break;
  case "Casual":
    $style = "Casual";
    break;
  case "Exotic":
    $style = "Exotic";
    break;
  case "Sophisticated":
    $style = "Sophisticated";
    break;
  case "Western":
    $style = "Western";
    break;
  case "Traditional":
    $style = "Traditional";
    break;
  case "Punk":
    $style = "Punk";
    break;
  case "Rocker":
    $style = "Rocker";
    break;
  case "Gothic":
    $style = "Gothic";
    break;
}
$subtype = filter_input(INPUT_POST, 'subtype');
$material = filter_input(INPUT_POST, 'material');
$dateOfPurchase = filter_input(INPUT_POST, 'dateOfPurchase');
$description = filter_input(INPUT_POST, 'description');
$available = filter_input(INPUT_POST, 'available');
$price = filter_input(INPUT_POST, 'price');
$size = filter_input(INPUT_POST, 'size');
        
$item = new \must\Clothing($conn);
$user_ID = $_SESSION["user_ID"];

if (isset($addMore))
{
  $set = $item->insertClothing($user_ID, $season, $brand, $type, $color, $style, $subtype, $material, $dateOfPurchase, $description, $price, $available, $size);
  if($set == true)
  {
    $_SESSION["itemAdded"]=TRUE;
    header("location: addItem.php");
  }
}
else
{
  $_SESSION["itemAdded"]=FALSE;
  header("location: addItem.php");
}

if (isset($generate))
{
  $set = $item->insertClothing($user_ID, $season, $brand, $type, $color, $style, $subtype, $material, $dateOfPurchase, $description, $price, $available, $size);
  if($set == true)
  {
  $_SESSION["itemAdded"]=TRUE;
  header("location: preference.php");
  }
}
else
{
  $_SESSION["itemAdded"]=FALSE;
  header("location: addItem.php");
}

?>
   </div>
   </html>