<!DOCTYPE html>
<?php
  session_start();
  require_once 'connect.php';
?>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="static/css/general.css" />
    <link rel="stylesheet" type="text/css" href="static/css/preference.css" />

    <script src="static/libs/jquery-2.1.4.js" ></script>

    <link rel='stylesheet' href='static/libs/spectrum/spectrum.css' />
    <script src='static/libs/spectrum/spectrum.js'></script>
    
    <title>Preferences</title>
  </head>
  <body>
    <div id="mainContainer">
      <div id='containerCenter'>
        <img src="static/img/beeldmerk_MuStLG.png" width="15%" align="right">
        <div class="form">
          <h1>Geef je voorkeur aan</h1>
          <h5><b class="requiredStar">*</b> verplicht</h5>
          <form method="post" action="match.php">
            <p>
            <label class="select">
            <select name="brand">
  <?php
              echo "<option></option>";
              $user_ID = $_SESSION['user_ID'];
              $sql = "SELECT brand FROM item INNER JOIN clothing ON item.item_ID = clothing.item_ID WHERE user_ID = '$user_ID' GROUP BY brand";

              $result = $conn->query($sql);

              // kijken of er resultaten zijn
              if ( $result->num_rows > 0 )
              {
                while($row = $result->fetch_assoc())
                {
                  $brand = $row['brand'];
                  echo '<option value="'.$brand.'">'.ucfirst(strtolower($brand)).'</option>';
                }
              }
  ?>
            </select></label><b class="requiredStar"></b> Merk</p>

            <p>
            <label class="select">
            <select name="style">
  <?php
              $sql = "SELECT style FROM item INNER JOIN clothing ON item.item_ID = clothing.item_ID WHERE user_ID = '$user_ID' GROUP BY style";

              $result = $conn->query($sql);

              // kijken of er resultaten zijn
              if ( $result->num_rows > 0 )
              {
                while($row = $result->fetch_assoc())
                {
                  $style = $row['style'];
                  echo '<option value="'.$style.'">'.ucfirst(strtolower($style)).'</option>';
                }
              }
  ?>
            </select></label><b class="requiredStar"> * </b>Style</p>

            <p style="margin-bottom: 30px">
            <label class="select">
            <select name="season">
  <?php
              $sql = "SELECT season FROM item INNER JOIN clothing ON item.item_ID = clothing.item_ID WHERE user_ID = '$user_ID' GROUP BY season";

              $result = $conn->query($sql);

              // kijken of er resultaten zijn
              if ( $result->num_rows > 0 )
              {
                while($row = $result->fetch_assoc())
                {
                  $season = $row['season'];
                  echo '<option value="'.$season.'">'.ucfirst(strtolower($season)).'</option>';
                }
              }
  ?>
            </select></label><b class="requiredStar"> * </b>Season</p>

            <script>
              var selected = false;
              function select(item) {
                if($('.'+item+'').attr('src') == 'static/img/icon_'+item+'.png')
                {
                  $('.'+item+'').attr('src', 'static/img/icon_'+item+'_selected.png');
                  $('.'+item+'Input').prop('checked', true);
                  selected = true;
                  $('#alert').html('');
                }
                else
                {
                  $('.'+item+'').attr('src', 'static/img/icon_'+item+'.png');
                  $('.'+item+'Input').prop('checked', false);
                  selected = false;
                }
              }
              function validate()
              {
                if(selected)
                {
                  $('form').submit();
                }
                else
                {
                  $("#alert").html('U heeft nog geen kledingtype aangeklikt');
                }
              }
            </script>

            <input class="imgCheckbox shirtInput" type="checkbox" name="shirt"/>
            <input class="imgCheckbox truiInput" type="checkbox" name="trui"/>
            <input class="imgCheckbox broekInput" type="checkbox" name="broek"/>
            <input class="imgCheckbox jasInput" type="checkbox" name="jas"/>
            <input class="imgCheckbox hoofddekselInput" type="checkbox" name="hoofddeksel"/>
            <input class="imgCheckbox overhemdInput" type="checkbox" name="overhemd"/>
            <input class="imgCheckbox schoenenInput" type="checkbox" name="schoenen"/>
            
            <div class="imgDiv">
            <img class="img shirt" src="static/img/icon_shirt.png" onclick="select('shirt')" alt="Shirt" title="Shirt"/>
            <img class="img trui" src="static/img/icon_trui.png" onclick="select('trui')" alt="Trui" title="Trui"/>
            <img class="img broek" src="static/img/icon_broek.png" onclick="select('broek')" alt="Broek" title="Broek"/>
            <img class="img jas" src="static/img/icon_jas.png" onclick="select('jas')" alt="Jas" title="Jas"/>
            </div>
            <div class="imgDiv">
            <img class="img hoofddeksel" src="static/img/icon_hoofddeksel.png" onclick="select('hoofddeksel')" alt="Hoofddeksel" title="Hoofddeksel"/>
            <img class="img overhemd" src="static/img/icon_overhemd.png" onclick="select('overhemd')" alt="Overhemd" title="Overhemd"/>
            <img class="img schoenen" src="static/img/icon_schoenen.png" onclick="select('schoenen')" alt="Schoenen" title="Schoenen"/>
            <b class="requiredStar"> * </b>
            </div>
            <div style="clear:both"></div>
            <div id="alert" style="height:30px"></div>

            <input class="input submit" type="submit" name="addMore" value="Meer toevoegen" formaction="addItem.php">
            <input class="input submit generate" type="button" name="generate" value="Match kleding" onclick="validate()"  >

          </form>
        </div>
      </div>
    </div>
  </body>
</html>