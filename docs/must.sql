-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 11, 2016 at 11:30 PM
-- Server version: 5.5.41-log
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `must`
--

-- --------------------------------------------------------

--
-- Table structure for table `clothing`
--

CREATE TABLE IF NOT EXISTS `clothing` (
`clothing_ID` int(11) NOT NULL,
  `item_ID` int(11) NOT NULL,
  `pattern` varchar(255) NOT NULL,
  `season` varchar(55) NOT NULL,
  `user_ID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=586102 ;

--
-- Dumping data for table `clothing`
--

INSERT INTO `clothing` (`clothing_ID`, `item_ID`, `pattern`, `season`, `user_ID`) VALUES
(586081, 781267, 'NONE', 'all', 626427),
(586082, 781268, 'NONE', 'zomer', 626427),
(586083, 781269, 'NONE', 'winter', 626427),
(586084, 781270, 'NONE', 'all', 626427),
(586085, 781271, 'NONE', 'lente', 626427),
(586086, 781272, 'NONE', 'lente', 626427),
(586087, 781273, 'NONE', 'all', 626427),
(586088, 781274, 'NONE', 'zomer', 626427),
(586089, 781275, 'NONE', 'all', 626427),
(586090, 781276, 'NONE', 'all', 626427),
(586091, 781277, 'NONE', 'zomer', 626427),
(586092, 781278, 'NONE', 'herfst', 626427),
(586093, 781279, 'NONE', 'all', 626427),
(586094, 781280, 'NONE', 'winter', 626427),
(586095, 781281, 'NONE', 'all', 626427),
(586096, 781282, 'NONE', 'all', 626427),
(586097, 781283, 'NONE', 'all', 626427),
(586098, 781284, 'NONE', 'all', 626427),
(586099, 781285, 'NONE', 'all', 626427),
(586100, 781286, 'NONE', 'all', 626427),
(586101, 781287, 'NONE', 'all', 626427);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
`item_ID` int(11) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `subtype` varchar(255) NOT NULL,
  `color` varchar(55) NOT NULL,
  `style` varchar(255) NOT NULL,
  `material` varchar(55) NOT NULL,
  `dateOfPurchase` date NOT NULL,
  `receipt` longblob,
  `description` varchar(255) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `price` float NOT NULL,
  `size` varchar(55) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=781288 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_ID`, `brand`, `type`, `subtype`, `color`, `style`, `material`, `dateOfPurchase`, `receipt`, `description`, `available`, `price`, `size`) VALUES
(781267, 'Diesel', 'shirt', '', 'black', 'Classic', 'katoen', '2015-10-09', NULL, 'Te gebruiken als ondershirt en normaal', 1, 15, 'M'),
(781268, 'Sting', 'shirt', '', 'navy', 'Traditional', 'katoen', '2015-10-09', NULL, 'Lievelingsshirt', 1, 20, 'M'),
(781269, 'Diesel', 'shirt', '', 'white', 'Casual', 'polyester', '2015-10-09', NULL, 'Cadeautje van mams', 1, 0, 'M'),
(781270, 'Zara', 'trui', '', 'grey', 'Casual', 'katoen en wol', '2015-10-09', NULL, 'Lekker warm', 1, 60, 'M'),
(781271, 'Diesel', 'trui', '', 'black', 'Classic', 'Viscose', '2015-10-09', NULL, 'Dunne trui', 1, 50, 'M'),
(781272, 'G-star', 'trui', '', 'teal', 'Exotic', 'Katoen', '2015-12-09', NULL, 'Met grijs vakje', 1, 45, 'M'),
(781273, 'Hilfiger', 'broek', 'pantalon', 'navy', 'Chic', 'polyester', '2015-12-09', NULL, 'Voor nette gelegenheden', 1, 90, 'M'),
(781274, 'Levi', 'broek', 'Korte broek', 'teal', 'Casual', 'Spijker', '2015-10-09', NULL, 'Rafelig', 1, 40, 'M'),
(781275, 'Diesel', 'broek', 'Lange broek', 'teal', 'Casual', 'Spijker', '2015-10-09', NULL, 'Standaard', 1, 110, 'M'),
(781276, 'Hilfiger', 'jas', '', 'navy', 'Chic', 'polyester', '2015-12-09', NULL, 'Voor nette gelegenheden', 1, 160, 'M'),
(781277, 'Nike', 'jas', 'Zomerjas', 'green', 'Casual', 'polyester', '2015-10-09', NULL, 'Sportief', 1, 40, 'M'),
(781278, 'Arma', 'jas', '', 'olive', 'Rocker', 'Leer', '2015-12-09', NULL, '', 1, 300, 'M'),
(781279, 'Nike', 'hoofddeksel', 'Pet', 'black', 'Rocker', 'polyester', '2015-10-09', NULL, 'Pleps', 1, 25, 'M'),
(781280, 'Diesel', 'hoofddeksel', 'Muts', 'maroon', 'Traditional', 'wol', '2015-10-09', NULL, 'Lekker warm', 1, 30, 'M'),
(781281, 'Monster', 'hoofddeksel', 'Pet', 'lime', 'Casual', 'polyester', '2015-10-09', NULL, 'Krijg ik energie van', 1, 0, 'M'),
(781282, 'Hilfiger', 'overhemd', 'lange mouwen', 'white', 'Chic', 'Zijde', '2015-12-09', NULL, 'Voor nette gelegenheden', 1, 125, 'M'),
(781283, 'WE', 'overhemd', 'korte mouwen', 'silver', 'Classic', 'Katoen', '2015-12-09', NULL, 'Uitverkoop', 1, 10, 'M'),
(781284, 'Sting', 'overhemd', '', 'navy', 'Classic', 'Katoen', '2015-10-09', NULL, 'Met vakjes', 1, 40, 'M'),
(781285, 'Hilfiger', 'schoenen', 'Instapper', 'brown', 'Chic', 'Leer', '2015-12-09', NULL, 'Voor nette gelegenheden', 1, 150, 'M'),
(781286, 'Nike', 'schoenen', 'Airmax', 'black', 'Casual', 'polyester', '2015-10-09', NULL, 'Sportief', 1, 110, 'M'),
(781287, 'Diesel', 'schoenen', '', 'white', 'Casual', 'Leer', '2015-10-09', NULL, 'Allround', 1, 90, 'M');

-- --------------------------------------------------------

--
-- Table structure for table `juwelery`
--

CREATE TABLE IF NOT EXISTS `juwelery` (
`juwelery_ID` int(11) NOT NULL,
  `item_ID` int(11) NOT NULL,
  `user_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_ID` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `name` varchar(255) NOT NULL,
  `subscribeDate` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=626428 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_ID`, `email`, `password`, `dateOfBirth`, `name`, `subscribeDate`) VALUES
(1, 'dnl.m@live.nl', '0', '1987-09-09', 'Daniel', '2016-05-05'),
(626427, 'henk@email.nl', '$2y$10$E1kj5/nbnyxhUtprX51RJeuXdZKZhLKNBZhsL30CxvgLQnzJPwICK', '0000-00-00', 'Henk', '2016-05-09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clothing`
--
ALTER TABLE `clothing`
 ADD PRIMARY KEY (`clothing_ID`), ADD KEY `item_ID` (`item_ID`), ADD KEY `user_ID` (`user_ID`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
 ADD PRIMARY KEY (`item_ID`);

--
-- Indexes for table `juwelery`
--
ALTER TABLE `juwelery`
 ADD PRIMARY KEY (`juwelery_ID`), ADD KEY `user_ID` (`user_ID`), ADD KEY `item_ID` (`item_ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clothing`
--
ALTER TABLE `clothing`
MODIFY `clothing_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=586102;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
MODIFY `item_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=781288;
--
-- AUTO_INCREMENT for table `juwelery`
--
ALTER TABLE `juwelery`
MODIFY `juwelery_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=626428;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `clothing`
--
ALTER TABLE `clothing`
ADD CONSTRAINT `clothing_ibfk_1` FOREIGN KEY (`item_ID`) REFERENCES `item` (`item_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `clothing_ibfk_2` FOREIGN KEY (`user_ID`) REFERENCES `users` (`user_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `juwelery`
--
ALTER TABLE `juwelery`
ADD CONSTRAINT `juwelery_ibfk_1` FOREIGN KEY (`user_ID`) REFERENCES `users` (`user_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `juwelery_ibfk_2` FOREIGN KEY (`item_ID`) REFERENCES `item` (`item_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
