<!DOCTYPE html>
<?php
  session_start();
  require_once 'connect.php';
  require_once 'objects\Item.php';
  require_once 'objects\Clothing.php';
  require_once 'objects\User.php';
?>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="static/css/general.css" />
    <link rel="stylesheet" type="text/css" href="static/css/preference.css" />
    
    <script src="static/libs/jquery-2.1.4.js" ></script>
    
    <link rel='stylesheet' href='static/libs/spectrum/spectrum.css' />
    <script src='static/libs/spectrum/spectrum.js'></script>
    
    <title>Matched</title>
  </head>
  <body>
    <div id="mainContainer">
      <div id='containerCenter'>
        <img src="static/img/beeldmerk_MuStLG.png" width="15%" align="right">
        
        <div class="form">
          <h1>Match</h1>
          <div style="clear:both"></div>
<?php
          $userId = $_SESSION['user_ID'];
          
          $type = array();
          
          $style = filter_input(INPUT_POST, "style");
          $brand = filter_input(INPUT_POST, "brand");
          $season = filter_input(INPUT_POST, "season");
          
          $shirt = filter_input(INPUT_POST, "shirt");
          $trui = filter_input(INPUT_POST, "trui");
          $broek = filter_input(INPUT_POST, "broek");
          $hoofddeksel = filter_input(INPUT_POST, "hoofddeksel");
          $overhemd = filter_input(INPUT_POST, "overhemd");
          $schoenen = filter_input(INPUT_POST, "schoenen");
          $jas = filter_input(INPUT_POST, "jas");
          
          $clothing = new must\Clothing($conn);
          
          $type['shirt'] = isset($shirt) ? true : false;
          $type['trui'] = isset($trui) ? true : false;
          $type['broek'] = isset($broek) ? true : false;
          $type['hoofddeksel'] = isset($hoofddeksel) ? true : false;
          $type['overhemd'] = isset($overhemd) ? true : false;
          $type['schoenen'] = isset($schoenen) ? true : false;
          $type['jas'] = isset($jas) ? true : false;
          
          if(isset($style))
          {
            foreach( $type as $typeKey => $value )
            {
              if( $value )
              {
                try
                {
                  $clothingItemArray = $clothing->getOneItemOnConditions( $userId, $typeKey, $style, $brand, $season );
                  if( !empty( $clothingItemArray ) )
                  {
                    $brand = $clothingItemArray['brand'];
                    $size = $clothingItemArray['size'];
                    $color = $clothingItemArray['color'];
                    $subtype = $clothingItemArray['subtype'];
                    $pattern = $clothingItemArray['pattern'];
                    $description = $clothingItemArray['description'];

                    $view = "<img class='img' src='static/img/icon_".$typeKey.".png' style='float:left;'/>"
                      . "<div style='display: flex; align-items: center; height:75px;'>"
                      . "<div style='float:left; margin:0 20px 0 20px; width:30%;'>"
                      . "<b>Merk: </b>" . ucfirst( strtolower( $brand ) ) . "<br>"
                      . "<b>Maat: </b> " . ucfirst( strtolower( $size ) ) . "<br>"
                      . "<b>Kleur: </b>" . ucfirst( strtolower( $color ) ) . "</div><div style='float:left'>"
                      . "<b>Subtype: </b>" . ucfirst( strtolower( $subtype ) ) . "<br>"
                      . "<b>Patroon: </b> " . ucfirst( strtolower( $pattern ) ) . "<br>"
                      . "<b>Omschrijving: </b> " . ucfirst( strtolower( $description ) ) . "</div><div style='clear:both'></div></div><br>";
                    echo $view;
                  }
                }
                catch(Exception $e)
                {
                  $exception = $e->getMessage();
                  echo $exception;
                }
              }
            }
          }
?>
        </div>
        <a href="http://localhost/mustlookgood/login.php" class="logout">Logout</a>
      </div>
    </div>
  </body>
</html>