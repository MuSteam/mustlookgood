<?php
session_start();
require_once 'connect.php';
?><!DOCTYPE html>

<html>
    <html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="static/css/general.css" />
    <link rel="stylesheet" type="text/css" href="static/css/preference.css" />
  
    <title>deleteItem</title>
  </head>
    <body>
    <div id="mainContainer">
      <div id='containerCenter'>
        <img src="static/img/beeldmerk_MuStLG.png" width="15%" align="right">
        <div class="form">
          <h1>Delete een item</h1>
          <table style="width:100%">
            <?php
            $user_ID = $_SESSION['user_ID'];
            $sql = "SELECT * FROM item INNER JOIN clothing ON item.item_ID = clothing.item_ID WHERE user_ID = '$user_ID' GROUP BY type";
            
            $items = array(array('henk', 50, 'smit'), array('klaas', 60, 'smit'),array('piet', 30, 'jansen'));
            
            foreach ($items as $item) 
            {
              echo "<tr>";
              if (is_array($item))
              {
                foreach ($item as $value) 
                {
                   echo "<td> ".$value." </td>";
                }
              }
              echo "</tr>";
            }

              $user_ID = $_SESSION['user_ID'];
              $sql = "SELECT brand FROM item INNER JOIN clothing ON item.item_ID = clothing.item_ID WHERE user_ID = '$user_ID' GROUP BY brand";

              $result = $conn->query($sql);

              // kijken of er resultaten zijn
              if ( $result->num_rows > 0 )
              {
                while($row = $result->fetch_assoc())
                {
                  $brand = $row['brand'];
                  echo '<option value="'.$brand.'">'.ucfirst(strtolower($brand)).'</option>';
                }
              }
            
              
              
            ?>
          </table>
        </div>
      </div>
    </div>
    </body>
</html>
