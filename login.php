<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC>
<?php require_once( "connect.php"); ?>
<?//php require_once ("static\js\datepicker.js"); ?>

<html>
<head>
  <link rel="stylesheet" type="text/css" href="static/css/general.css" />
  <link rel="stylesheet" type="text/css" href="static/css/login.css" />
</head>

<body>
  <div id="mainContainer">
    <div id='containerCenter'>
      <img src="static/img/beeldmerk_MuStLG.png" width="15%" align="right">
      <div class="register">
        <h1>Inloggen</h1>
        <form action="loginValidate.php" method="POST">
            <p><input type="email" name="email" value="
                  <?php if(isset($_SESSION["email"]))
                  {
                    echo $_SESSION["email"];
                  }
                  ?> 
        " placeholder="E-mail" required autofocus />
                  <?php 
                  if (isset($_SESSION["email"]) && $_SESSION["wrongMail"])
                  {
                    echo "<br>het ingevoerde emailadres is onjuist";
                  }
                  ?></p>
          <p><input type="password" name="passwordLogin" value="" placeholder="Wachtwoord" required />
                  <?php 
                  if (isset($_SESSION["email"]) && !$_SESSION["wrongMail"])
                  {
                    echo "<br>het ingevoerde wachtwoord is onjuist";
                  }
                  if (isset($_SESSION["wrongMail"])){
                    session_unset($_SESSION["wrongMail"]);
                  }
                  ?></p>
          <p><input type="submit" name="login" value="Inloggen" /></p>
        </form>

        <h1>Registreren</h1>
        <form action="register.php" method="POST">
          <p><input type="text" name="name" value="" placeholder="Naam" required /></p>
          <p><input type="text" name="dateOfBirth" placeholder="geboortedatum (dd-mm-yyyy)" pattern="[0-3]{1}[0-9]{1}-[0|1]{1}[0-9]{1}-[1|2]{1}[0|9]{1}[0-9]{2}" title="dd-mm-yyyy"/></p>
          <p><input type="email" name="email" placeholder="E-mail adres" required /></p>
          <?php 
          if(isset($_SESSION["emailExists"]) && $_SESSION["emailExists"]){
            echo"E-mail is reeds geregistreerd."; 
            session_unset($_SESSION["emailExists"]);
          }
          ?>
          <p><input type="password" name="password" placeholder="Wachtwoord" required /></p>
          <p><input type="password" name="password" placeholder="Wachtwoord herhalen" required /></p> <!-- Controle check  nog toevoegen. -->
          <p><input type="submit" name="register" value="Account aanmaken" /></p>
        </form>
      </div>

      <div style="clear:both;"></div>
    </div>
  </div>
</body>
</html>
